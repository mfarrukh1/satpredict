import dash_core_components as dcc
import dash_html_components as html
import dash_daq as daq
from datetime import datetime, timezone, timedelta

MAPBOX_ACCESS_TOKEN = "pk.eyJ1IjoibWZhcnJ1a2gxIiwiYSI6ImNrOGJibWxoOTA0ZXMzZnA5cWZjMmVyeXkifQ.tgy73bMzjqA-QtyiinsJvQ"
#MAPBOX_STYLE = "mapbox://styles/mfarrukh1/ck8bbuczs1vy61jp2nwcm5k8l"       # Mono - Blue
MAPBOX_STYLE = "mapbox://styles/mfarrukh1/ck8byd3ht1cov1imkfl2k1vfe"        # Mono - Black

map_data = [
    {
        "type": "scattermapbox",
        "lat": [0],
        "lon": [0],
        "hoverinfo": "lon+lat",
        "text": "Satellite Path",
        "mode": "lines",
        "line": {"width": 2, "color": "#ffe102"},
    },
    {
        "type": "scattermapbox",
        "lat": [0],
        "lon": [0],
        "hoverinfo": "text+lon+lat",
        "text": "PRSS-1",
        "mode": "markers",
        "marker": {"size": 10, "color": "#ffe102"},
    },
    {
        "type": "scattermapbox",
        "lat": [24.8607],
        "lon": [67.0011],
        "hoverinfo": "text+lon+lat",
        "text": "Karachi",
        "mode": "markers",
        "marker": {"size": 10, "color": "#ff0000"},
    },
    {
        "type": "scattermapbox",
        "lat": [33.6844],
        "lon": [73.0479],
        "hoverinfo": "text+lon+lat",
        "text": "Islamabad",
        "mode": "markers",
        "marker": {"size": 10, "color": "#ff0000"},
    },
]

map_layout = {
    "mapbox": {
        "accesstoken": MAPBOX_ACCESS_TOKEN,
        "style": MAPBOX_STYLE,
        "center": {"lat": 45},
        "maxBounds": [ [-180, -85], [180, 85] ],
    },
    "showlegend": False,
    "autosize": True,
    "paper_bgcolor": "#1e1e1e",
    "plot_bgcolor": "#1e1e1e",
    "margin": {"t": 0, "r": 0, "b": 0, "l": 0},
}

map_toggle = daq.ToggleSwitch(
    id="control-panel-toggle-map",
    value=False,
    label=["Hide path", "Show path"],
    color="#ffe102",
    style={"color": "#black"},
)

satellite_dropdown_text = html.P(
    id="satellite-dropdown-text", children=["PRSS-1 Satellite Tracking"]
)

map_graph = html.Div(
    id="world-map-wrapper",
    children=[
        map_toggle,
        dcc.Graph(
            id="world-map",
            figure={"data": map_data, "layout": map_layout},
            config={"displayModeBar": False, "scrollZoom": False},
        ),
    ],
)

predictions = html.P(id="predictions-text", children=["Predictions will load here!"])

layout1 = html.Div([
    dcc.Interval(id="interval", interval=1 * 5000, n_intervals=0),
    satellite_dropdown_text,
    map_graph,
    predictions
])

