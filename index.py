import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash

from app import app
from layouts import layout1, map_data, predictions

from skyfield.api import Topos, load
from skyfield.api import EarthSatellite
from datetime import datetime,timezone,timedelta
import math
import json
import urllib.request

app.layout = layout1

# download tle (json object) from n2yo API 
prss1_url = 'https://www.n2yo.com/rest/v1/satellite/tle/43530&apiKey=PF4B22-3FDKDZ-92S7SX-4C2E'
data = urllib.request.urlopen(prss1_url).read().decode()
obj = json.loads(data)
tle = obj['tle'].split('\n')

predictions_string = ""
satellite = EarthSatellite(tle[0], tle[1], name='PRSS-1', ts=None)
ts = load.timescale()
now_utc = datetime.now(timezone.utc)
t = ts.utc(now_utc)
geocentric = satellite.at(t)
subpoint = geocentric.subpoint()
map_data[1]["lat"] = [math.degrees(subpoint.latitude.radians)]
map_data[1]["lat"] = [math.degrees(subpoint.longitude.radians)]

bluffton = Topos('24.926294 N', '67.022095 E')
now_utc = datetime.now(timezone.utc)
t0 = ts.utc(now_utc)
t1 = ts.utc(now_utc + timedelta(days=1))
t, events = satellite.find_events(bluffton, t0, t1, altitude_degrees=3.0)
for ti, event in zip(t, events):
    name = ('rise above 3°', 'culminate', 'set below 3°')[event]
    predictions_string = predictions_string + " >.< " + str(ti.utc_jpl()) + " " + str(name)
#print (predictions_string)

@app.callback(
    
    Output("world-map","figure"),
    [
        Input("interval", "n_intervals"),
        Input("control-panel-toggle-map", "value")
    ],
    [State("world-map", "figure")],
    )
def update_marker(n_intervals, value, old_figure):
    ctx = dash.callback_context
    figure = old_figure
    trigger_input = ctx.triggered[0]["prop_id"].split(".")[0]
    if trigger_input == "control-panel-toggle-map":
        if value:
            orbit = getCurrentOrbit()
            figure["data"][0]["lat"] = orbit[0]
            figure["data"][0]["lon"] = orbit[1]
            return figure
        else:
            figure["data"][0]["lat"] = [0]
            figure["data"][0]["lon"] = [0]
            return figure
    now_utc = datetime.now(timezone.utc)
    t = ts.utc(now_utc)
    geocentric = satellite.at(t)
    subpoint = geocentric.subpoint()
    lat = "{0:09.4f}".format(math.degrees(subpoint.latitude.radians))
    lon = "{0:09.4f}".format(math.degrees(subpoint.longitude.radians))
    figure["data"][1]["lat"] = [lat]
    figure["data"][1]["lon"] = [lon]
    figure["layout"]["mapbox"]["center"] = {"lat": lat, "lon": lon}
    return figure


@app.callback(Output("predictions-text", "children"), [Input("predictions-text", "n_clicks")])
def on_click(number_of_times_button_has_clicked):
    global predictions_string
    #print ("Click")
    return predictions_string

def getCurrentOrbit():
    orbitLat = []
    orbitLon = []
    index = 0
    global satellite
    ts = load.timescale()
    now_utc = datetime.now(timezone.utc)
    for i in range(0, 3000, 10):
        t = ts.utc(now_utc + timedelta(seconds=i))
        geocentric = satellite.at(t)
        subpoint = geocentric.subpoint()
        orbitLat.append("{0:09.4f}".format(math.degrees(subpoint.latitude.radians)))
        orbitLon.append("{0:09.4f}".format(math.degrees(subpoint.longitude.radians)))
        index += 1
    return [orbitLat, orbitLon]

if __name__ == '__main__':
    app.run_server(debug='False',port=8080,host='0.0.0.0')
